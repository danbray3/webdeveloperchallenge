@extends('layouts.app')

@section('content')
<div class="container product-container">
    <h2 class="pt-3">ADMI Gaming PC Package</h2>
    <div class="center-vertically">
        <img src="images/pc.jpg" alt="PC" class="w-25 float-left">
        <ul>
            <li>An ideal PC for mid to high-end gaming, with Fast Quad Core AMD Ryzen 2300X Processor</li>
            <li>1TB Hard Drive / 8GB DDR4 RAM / Fully decked Tempered Glass RGB LED Gaming PC Case / 500W Power Supply / package includes 7 Colour Illuminated Gaming Keyboard, Mouse, Mat & Headset & 24 Inch LED Monitor</li>
            <li>NVIDIA GTX 1050Ti 4GB DDR5 HDMI Graphics Card - - Play games with high FPS and high settings!</li>
            <li>With Windows 10 Operating System - Microsofts most stable and feature-packed OS to date - meaning you are ready-to-go straight out of the box!</li>
        </ul>
    </div>
    <div class="center-vertically">
        <div class="float-left">
            <p>RRP:&#09;£899.95</p>
            <p>Price:&#09;<b>£620</b></p>
        </div>
        <form action="/buy" method="post" class="pl-3 pb-3">
            @csrf
            <div>
                <button class="btn btn-primary">Buy</button>
            </div>
        </form>
    </div>
{{--    Not working on localhost--}}
    <div class="sharethis-inline-share-buttons"></div>
</div>
@endsection
