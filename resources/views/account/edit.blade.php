@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/account/update" enctype="multipart/form-data" method="post">
        @csrf
        @method('PATCH')
        <div class="row">
            <div class="col-8 offset-2">
                <div class="row">
                    <h1>Edit Profile</h1>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label">Name</label>

                    <input id="name"
                           type="text"
                           class="form-control @error('name') is-invalid @enderror"
                           name="name"
                           value="{{ old('name') ?? $user->name }}"
                           autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="address" class="col-md-4 col-form-label">Address</label>

                    <input id="address"
                           type="text"
                           class="form-control @error('address') is-invalid @enderror"
                           name="address"
                           value="{{ old('address') ?? $user->address }}"
                           autocomplete="address" autofocus>
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="postcode" class="col-md-4 col-form-label">Postcode</label>

                    <input id="postcode"
                           type="text"
                           class="form-control @error('postcode') is-invalid @enderror"
                           name="postcode"
                           value="{{ old('postcode') ?? $user->postcode }}"
                           autocomplete="postcode" autofocus>
                    @error('postcode')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>

                    <input id="email"
                           type="text"
                           class="form-control @error('email') is-invalid @enderror"
                           name="email"
                           value="{{ old('email') ?? $user->email }}"
                           autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>

                    <input id="password"
                           type="password"
                           class="form-control @error('password') is-invalid @enderror"
                           name="password"
                           autocomplete="email" autofocus>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label">{{ __('Confirm Password') }}</label>

                    <input id="password-confirm"
                           type="password"
                           class="form-control @error('password') is-invalid @enderror"
                           name="password_confirmation"
                           autocomplete="email" autofocus>
                </div>

                <div class="row pt-4">
                    <button class="btn btn-primary">Save Profile</button>
                </div>

            </div>
        </div>
    </form>
</div>
@endsection
