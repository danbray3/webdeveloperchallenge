@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Make Payment</div>

                    <div class="card-body">
                        <form action="./payment/charge" method="post" id="payment-form">
                            @csrf
                            <label class="align-content-center">£620 - ADMI Gaming PC Package</label><br/>
                            <label for="card-element" class="align-content-center">Credit or debit card</label>
                            <div class="form-group row">

                                <div class="col-md-6">
                                    <div id="card-element" class="align-content-center">
                                        <!-- a Stripe Element will be inserted here. -->
                                    </div>
                                </div>
                            </div>
                            <div id="card-errors" class="alert-danger">{{ session('status-error') }}</div>
                            <div class="alert-info">{{ session('status-ok') }}</div>
                            <br/>
                            <button>Submit Payment</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="./js/charge.js"></script>
@endsection
