<?php

namespace App\php;

class Stripe implements Payment
{
    public function sendPayment($params): array
    {
        \Stripe\Stripe::setApiKey('sk_test_gP1927NHeku6zxyIJLtExcMF00an4C8AYS');
        $token = $params['stripeToken'];
        $result = [];
        if ($token === session('stripeToken')) {
            $result['success'] = false;
            $result['failureMessage'] = 'Error! Payment already sent.';
            return $result;
        }
        session(['stripeToken' => $token]);
        try {
            $charge = \Stripe\Charge::create(
                array(
                    'amount' => 620,
                    'currency' => 'gbp',
                    'source' => $token
                )
            );
        }
        catch (\Stripe\Exception\InvalidRequestException $e) {
            $result['success'] = false;
            $result['failureMessage'] = 'Error! Error sending payment.';
            return $result;
        }
        $result = [];
        $result['success'] = $charge->status === 'succeeded';
        $result['failureMessage'] = $charge->failure_message;
        return $result;
    }
}
