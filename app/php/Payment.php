<?php

namespace App\php;

interface Payment
{
    public function sendPayment($params): array;
}
