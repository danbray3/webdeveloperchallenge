<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $user = auth()->user();
        return view('account.edit', compact('user'));
    }

    public function update()
    {
        if (request()->get('password')) {
            $data = request()->validate([
                'name' => 'required',
                'address' => 'required',
                'postcode' => 'required',
                'email' => 'required',
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            auth()->user()->update([
                'name' => $data['name'],
                'address' => $data['address'],
                'postcode' => $data['postcode'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
        }
        else {
            $data = request()->validate([
                'name' => 'required',
                'address' => 'required',
                'postcode' => 'required',
                'email' => 'required',
            ]);
            auth()->user()->update($data);
        }
        return redirect("/account");
    }
}
