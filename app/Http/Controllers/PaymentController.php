<?php

namespace App\Http\Controllers;

use App\php\Stripe;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function charge(Request $request) {
        $stripe = new Stripe();
        $params = [];
        $params['stripeToken'] = $request->get('stripeToken');
        $params['user'] = auth()->user();
        $result = $stripe->sendPayment($params);
        if ($result['success'] !== true) {
            return back()->with("status-error", $result['failureMessage']);
        }
        return back()->with("status-ok", 'Payment was successful. You should receive your order in 3-5 working days.');

    }

    public function index()
    {
        return view('payment/index');
    }
}
