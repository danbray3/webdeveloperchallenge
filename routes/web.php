<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('index');
Route::post('/buy', 'HomeController@buy');

Route::get('/payment', 'PaymentController@index');
Route::post('/payment/charge', 'PaymentController@charge');

Route::get('/account', 'AccountController@edit')->name('account.edit');
Route::patch('/account/update', 'AccountController@update')->name('account.update');
